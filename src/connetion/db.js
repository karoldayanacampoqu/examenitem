const mongoose = require("mongoose");
const mongo = require("../config/config.js");
const connectDB = async () => {
    const conexionUrl = mongo.conccionBD.mongodbrurl
    await mongoose.connect(conexionUrl);
    console.log("conexion a mongodb atlas exitosa");

};

module.exports = connectDB;