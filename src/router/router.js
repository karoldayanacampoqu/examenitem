const express = require("express");
const rutas = express.Router();
const Item = require("../models/Itemmodels.js");

const connectDB = require("../connetion/db.js");
connectDB();

rutas.get("/items", async (req, res) => {
    try {
        const item = await Item.find()
        res.json(item);
    } catch (error) {
        res.json({"error": error.message})
    }
});
rutas.post("/items", async (req, res) => {
    const { nombre, descripcion } = req.body;
    const nuevoItem = new Item({
        nombre,
        descripcion
    })
    await nuevoItem.save();
    res.json({ "mensaje": "Elemento guardado" });
});
rutas.put("/items/:id", async (req, res) => {
    const ItemId = req.params.id;
    const { nombre, descripcion } = req.body;
    const Itemecontrado = await Item.findById(ItemId);
    Itemecontrado.nombre = nombre;
    Itemecontrado.descripcion = descripcion;
    await Itemecontrado.save();
    res.json(Itemecontrado);

});
rutas.delete("/items/:id", async (req, res) => {
    const ItemId = req.params.id;
    const Itemecontrado = await Item.findById(ItemId);
    await Itemecontrado.deleteOne();
    res.json({"mensaje":"nombre eliminado"})

});

module.exports = rutas;