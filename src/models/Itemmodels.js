const mongoose = require("mongoose");

const ItemSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
    },
    descripcion: {
        type: String,
        requiered: true,
    }
});
const Item = mongoose.model("Item", ItemSchema);
module.exports = Item;