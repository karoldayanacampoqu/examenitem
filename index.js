const express = require("express");

const app = express();
const config = require("./src/config/config.js");
const rutasItem = require("./src/router/router.js");

app.use(express.json());
app.use("/apiitem", rutasItem);
app.listen(config.server.port, () => {

    console.log("servidor arriba" + config.server.port);
});
    


